package fr.rcdsm.nicolasklein.o_desc.data;

import java.util.Date;
import java.util.List;

/**
 * Created by orion on 18/03/2015.
 */
public abstract class PhysicalThing {
    private static List<PhysicalThing> rootContexts;
    protected String uniqueID;
    protected String parentID;
    protected String name;
    protected Date dateCreation;
    protected String jsonSourcePath;
    protected Date lastAccess;
    protected String type;

    public static List<PhysicalThing> getRootContexts() {
        return rootContexts;
    }
    public String getUniqueID() {
        return uniqueID;
    }
    public String getParentID() {
        return parentID;
    }
    public String getName() {
        return name;
    }
    public Date getDateCreation() {
        return dateCreation;
    }
    public String getJsonSourcePath() {
        return jsonSourcePath;
    }
    public Date getLastAccess() {
        return lastAccess;
    }
    public String getType() {
        return type;
    }

    protected void generateID(){

    }

    public static PhysicalThing[] searchFor(String[] words){
        return null;
    }

    public void moveUnder(PhysicalThing newParent){
    }

    public void save(){

    }

    public void load(){

    }
}
