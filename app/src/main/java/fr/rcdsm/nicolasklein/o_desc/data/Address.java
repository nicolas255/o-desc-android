package fr.rcdsm.nicolasklein.o_desc.data;

/**
 * Created by orion on 18/03/2015.
 */
public class Address {
    protected String streetLine1;
    protected String streetLine2;
    protected String city;
    protected String zipCode;
    protected String country;
}
