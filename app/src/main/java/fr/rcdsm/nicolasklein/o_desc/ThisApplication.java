package fr.rcdsm.nicolasklein.o_desc;

import android.app.Activity;
import android.app.Application;
import android.content.Context;

/**
 * Created by orion on 30/07/2015.
 */
public class ThisApplication extends Application {
	static private Context context;

	@Override
	public void onCreate() {
		super.onCreate();
		ThisApplication.context = getApplicationContext();
	}

	public static Context getAppContext(){
		return ThisApplication.context;
	}
}
