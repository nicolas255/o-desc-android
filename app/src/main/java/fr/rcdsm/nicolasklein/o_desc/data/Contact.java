package fr.rcdsm.nicolasklein.o_desc.data;

/**
 * Created by orion on 18/03/2015.
 */
public class Contact {
    protected String title;
    protected String nameLast;
    protected String nameFirst;
    protected String phone;
    protected String mail;
}
