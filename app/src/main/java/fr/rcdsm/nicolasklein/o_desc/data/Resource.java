package fr.rcdsm.nicolasklein.o_desc.data;

/**
 * Created by orion on 18/03/2015.
 */
public abstract class Resource extends PhysicalThing {
    protected String resourcePath;

    @Override
    public void load() {
        super.load();
    }

    @Override
    public void save() {
        super.save();
    }
}
