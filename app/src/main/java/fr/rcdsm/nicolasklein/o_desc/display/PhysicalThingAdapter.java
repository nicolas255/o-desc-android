package fr.rcdsm.nicolasklein.o_desc.display;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import fr.rcdsm.nicolasklein.o_desc.R;
import fr.rcdsm.nicolasklein.o_desc.data.PhysicalThing;
import fr.rcdsm.nicolasklein.o_desc.data.RealContext;
import fr.rcdsm.nicolasklein.o_desc.data.RealObject;
import fr.rcdsm.nicolasklein.o_desc.data.Resource;

public class PhysicalThingAdapter extends ArrayAdapter<PhysicalThing> {

	public PhysicalThingAdapter(Context context, int resource, List<PhysicalThing> objects) {
		super(context, resource, objects);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;

		PhysicalThing currentPhysicalThing = getItem(position);

		if(currentPhysicalThing != null){
			TextView textViewName = new TextView(getContext());
			if(currentPhysicalThing.getClass() == RealObject.class){
				if(view == null){
					view = LayoutInflater.from(getContext()).inflate(R.layout.list_view_item_real_object, null);
				}
				textViewName = (TextView) view.findViewById(R.id.list_view_item_real_object_name);
			}
			if(currentPhysicalThing.getClass() == RealContext.class){
				if(view == null){
					view = LayoutInflater.from(getContext()).inflate(R.layout.list_view_item_real_context, null);
				}
				textViewName = (TextView) view.findViewById(R.id.list_view_item_real_context_name);
			}
			if(currentPhysicalThing.getClass() == Resource.class){
				if(view == null){
					view = LayoutInflater.from(getContext()).inflate(R.layout.list_view_item_resource, null);
				}
				textViewName = (TextView) view.findViewById(R.id.list_view_item_resource_name);
			}
			textViewName.setText(currentPhysicalThing.getName());
		}

		return super.getView(position, convertView, parent);
	}
}
