package fr.rcdsm.nicolasklein.o_desc;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import fr.rcdsm.nicolasklein.o_desc.data.PhysicalThing;
import fr.rcdsm.nicolasklein.o_desc.display.PhysicalThingAdapter;


public class MainActivity extends AppCompatActivity {

	static public MainActivity mainInstance;

    private EditText editTextSearch;
    private ImageButton imageButtonClear;
    private ListView listViewPhysicalThings;

    private List<PhysicalThing> currentPhysicalThingList;
	private PhysicalThingAdapter physicalThingAdapter;

	public static MainActivity getMainInstance() {
		return mainInstance;
	}

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

		currentPhysicalThingList = new ArrayList<>();

		mainInstance = this;

		editTextSearch = (EditText) findViewById(R.id.search_box_edittext);
		imageButtonClear = (ImageButton) findViewById(R.id.search_box_clear_imagebutton);
		listViewPhysicalThings = (ListView) findViewById(R.id.physical_things_listview);

		imageButtonClear.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				editTextSearch.setText("");
			}
		});

		physicalThingAdapter = new PhysicalThingAdapter(this, listViewPhysicalThings.getId(), currentPhysicalThingList);
		listViewPhysicalThings.setAdapter(physicalThingAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.add_remove_move, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.take_new_resource) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
